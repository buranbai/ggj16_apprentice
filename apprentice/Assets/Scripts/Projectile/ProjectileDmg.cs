﻿using UnityEngine;
using System.Collections;

public class ProjectileDmg : MonoBehaviour {

	public float dmgPerHit = 5f;
	public ObjectType objectType;
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{

		if (canMelee (col)) 
		{
			Debug.Log ("Melee");
		}



	}

	private bool canMelee(Collider2D col)
	{
		DmgReceiver dmgReceiver = col.gameObject.GetComponent<DmgReceiver> ();
		if (dmgReceiver != null) 
		{
			if (dmgReceiver.getType () != objectType) {
				dmgReceiver.receiveDamage (dmgPerHit);
				Destroy (this.gameObject);
			}
			return true;
		}

		return false;
	}
}
