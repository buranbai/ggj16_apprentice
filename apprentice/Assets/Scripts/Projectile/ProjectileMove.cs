﻿using UnityEngine;
using System.Collections;

public class ProjectileMove : MonoBehaviour {

	public Vector2 direction;
	public Transform target;
	public bool homing = true;
	public float velocity = 2f;
	// Use this for initialization
	void Start () {
		target =	GetComponent<Projectile> ().target;
	}
	
	// Update is called once per frame
	void Update () {

		if (homing)
			calcDirection ();
		transform.Translate(direction * velocity *Time.deltaTime);
	}

	void calcDirection()
	{
		Vector2 distance = this.target.transform.position - this.transform.position;

		direction = distance.normalized;

	}
}
