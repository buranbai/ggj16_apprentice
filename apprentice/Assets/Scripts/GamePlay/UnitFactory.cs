﻿using UnityEngine;
using System.Collections;

public class UnitFactory : MonoBehaviour
{

	public GameObject[] unitAssets;
	public Transform spawnPositon;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	public void spawnUnit(UnitType unitType)
	{
		GameObject.Instantiate (unitAssets [(uint)unitType], spawnPositon.position, this.transform.localRotation);
	}
}

