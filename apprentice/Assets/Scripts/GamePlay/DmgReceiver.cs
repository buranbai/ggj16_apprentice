﻿using UnityEngine;
using System.Collections;

abstract public class DmgReceiver : MonoBehaviour , IDamageReceiver
{
	public ObjectType worldObjectType;
	public float health = 100.0f;

	abstract public bool receiveDamage(float dmg);


	public ObjectType getType()
	{
		return worldObjectType;	
	}

	protected bool checkLife()
	{
		if (health < 0) {
			destroy ();	
			return true;
		} else
			return false;
	}

	virtual protected void destroy()
	{
		
	}
}


