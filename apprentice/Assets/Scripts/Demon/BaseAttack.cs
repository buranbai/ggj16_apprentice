﻿using UnityEngine;
using System.Collections;

public class BaseAttack : MonoBehaviour
{
	protected DmgReceiver meleeDgmReceiver;
	protected RangeDmgReicever rangeDmgReceiver;
	protected AirDmgReceiver airDmgReceiver;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	virtual	public void onTouch (DmgReceiver dmgReceiver)
	{
		meleeDgmReceiver = dmgReceiver;
		meleeAttack ();
	}

	virtual public void onRange (RangeDmgReicever dmgReceiver)
	{
		rangeDmgReceiver = dmgReceiver;
		rangeAttack ();
	}

	virtual public void onOver(AirDmgReceiver dmgReceiver)
	{
		airDmgReceiver = dmgReceiver;
		airAttack ();
	}

	virtual	protected void meleeAttack ()
	{
	}

	virtual protected void rangeAttack ()
	{
	}

	virtual protected void airAttack()
	{
	}

}

