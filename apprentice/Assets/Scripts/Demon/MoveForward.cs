﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {

	public float defaultSpeed = 2f;
	private float speed = 2f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	

		this.transform.Translate (speed * Time.deltaTime * Vector2.right);
	}

	public void startMove()
	{
		this.speed = this.defaultSpeed;	
	}

	public void stop()
	{
		speed = 0f;
	}
}
