﻿using UnityEngine;
using System.Collections;

public class MoveintoAir : MonoBehaviour {

	public float velocity = 2f;
	public float height = 4f;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(this.transform.position.y < height)
			this.transform.Translate (velocity * Time.deltaTime * Vector2.up);
		else {
			
			Vector3 pos = this.transform.position;
			pos.y = height;
			this.transform.position = pos;
			this.GetComponent<MoveForward> ().enabled = true;
			this.enabled = false;
		}
	}
}
