﻿using UnityEngine;
using System.Collections;

public class AirAttack : BaseAttack
{
	public float period = .33f;
	public float dmgPerHit= 1f;
	public GameObject projectilePrefab;

	public void startAttack()
	{
		this.GetComponent<BoxCollider2D> ().enabled = false;
		InvokeRepeating ("attacking", 1f, period);
	}

	public void stopAttack()
	{
		this.GetComponent<BoxCollider2D> ().enabled = true;
		CancelInvoke ("attacking");
	}

	public void attacking()
	{
		if (this.meleeDgmReceiver != null) {
			this.meleeDgmReceiver.receiveDamage (dmgPerHit);

		} 
		else 
		{
			
			stopAttack ();
		}
	}

	override protected void meleeAttack ()
	{
		startAttack ();
	}
		

	override protected void airAttack()
	{
		startAirAttack ();
	}

	public void startAirAttack()
	{
		stopAirAttack ();
		InvokeRepeating ("airAttacking", 0f, period);
	}

	public void stopAirAttack()
	{

		CancelInvoke ("airAttacking");


	}

	public void airAttacking()
	{
		if (this.airDmgReceiver != null) {

				GameObject.Instantiate (projectilePrefab, this.transform.position, Quaternion.identity) ;


		} 
		else 
		{
			stopAirAttack ();
		}
	}


}

