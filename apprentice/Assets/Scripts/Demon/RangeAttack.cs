﻿using UnityEngine;
using System.Collections;

public class RangeAttack : BaseAttack
{
	public GameObject projectilePrefab;
	public float period = .33f;
	public float dmgPerHit= 1f;
	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void startAttack()
	{
		this.GetComponent<BoxCollider2D> ().enabled = false;
		InvokeRepeating ("attacking", 1f, period);
	}

	public void stopAttack()
	{
		this.GetComponent<BoxCollider2D> ().enabled = true;
		MoveForward moveForward = GetComponent<MoveForward> ();
		moveForward.startMove ();
		//this.GetComponent<MoveForward>().startMove();
	}

	public void attacking()
	{
		if (this.meleeDgmReceiver != null) {
			this.meleeDgmReceiver.receiveDamage (dmgPerHit);

		} 
		else 
		{
			CancelInvoke ("attacking");
			stopAttack ();
		}
	}

	override protected void meleeAttack()
	{
		stopRangeAttack ();
		startAttack ();
	}

	override protected void rangeAttack()
	{
		startRangeAttack ();
	}


	public void startRangeAttack()
	{

		InvokeRepeating ("rangeAttacking", .33f, period);
	}

	public void stopRangeAttack()
	{

		CancelInvoke ("rangeAttacking");

	}

	public void rangeAttacking()
	{
		if (this.rangeDmgReceiver != null) {
			GameObject projectile = 
				GameObject.Instantiate (projectilePrefab, this.transform.position, Quaternion.identity) 
					as GameObject;
			Projectile projectileMB = projectile.GetComponent<Projectile> ();
			projectileMB.target = this.rangeDmgReceiver.getTarget ();

		} 
		else 
		{
			CancelInvoke ("rangeAttacking");
			stopRangeAttack ();
		}
	}
}

