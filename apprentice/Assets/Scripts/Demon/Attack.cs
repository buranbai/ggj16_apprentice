﻿using UnityEngine;
using System.Collections;

namespace demon
{
	public class Attack : BaseAttack {


		public float period = .33f;
		public float dmgPerHit= 1f;

		override protected void meleeAttack(){
			startAttack ();
		}

	public void startAttack()
	{
			this.GetComponent<BoxCollider2D> ().enabled = false;
			InvokeRepeating ("attacking", 1f, period);
	}

		public void stopAttack()
		{
			this.GetComponent<BoxCollider2D> ().enabled = true;
			MoveForward moveForward = GetComponent<MoveForward> ();
			moveForward.startMove ();
			//this.GetComponent<MoveForward>().startMove();
		}

		public void attacking()
		{
			if (this.meleeDgmReceiver != null) {
				this.meleeDgmReceiver.receiveDamage (dmgPerHit);

			} 
			else 
			{
				CancelInvoke ("attacking");
				stopAttack ();
			}
		}
	
}
}