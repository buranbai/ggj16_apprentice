﻿using UnityEngine;
using System.Collections;

namespace demon
{
public class Demon : DmgReceiver
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	override public bool receiveDamage(float dmg)
	{
			return true;
		}

	void OnTriggerEnter2D(Collider2D col)
	{
	
			if (canMelee (col)) 
			{
				Debug.Log ("Melee");
			}
			else if (canRange (col)) 
			{
				Debug.Log("Range");
			} 
			else if(canAir(col))
			{
				Debug.Log("Air");	
			}

		
	}

		private bool canMelee(Collider2D col)
		{
			DmgReceiver dmgReceiver = col.gameObject.GetComponent<DmgReceiver> ();
			if (dmgReceiver != null) 
			{
				if (dmgReceiver.getType () != getType ()) {
					this.GetComponent<MoveForward> ().stop ();
					this.GetComponent<BaseAttack> ().onTouch (dmgReceiver);
				}
				return true;
			}

			return false;
		}

		private bool canAir(Collider2D col)
		{
			AirDmgReceiver airDmgReceiver = col.gameObject.GetComponent<AirDmgReceiver> ();
			if (airDmgReceiver != null) {
				if (airDmgReceiver.getType () != getType ()) {
					this.GetComponent<BaseAttack> ().onOver (airDmgReceiver);
				}
				return true;
			}
			return false;
			
		}

		private bool canRange(Collider2D col)
		{
			RangeDmgReicever rangeDmgReceiver = col.gameObject.GetComponent<RangeDmgReicever> ();
			if (rangeDmgReceiver != null) {
				if (rangeDmgReceiver.getType () != getType ()) {
					this.GetComponent<BaseAttack> ().onRange (rangeDmgReceiver);
				}
				return true;
			}
			return false;
		}

		void OnBecameInvisible()
		{
			Destroy (this.gameObject);
		}
}

}