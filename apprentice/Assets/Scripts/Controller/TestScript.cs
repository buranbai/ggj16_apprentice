﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour
{
	public UnitType unitType = UnitType.NONE;
	public UnitFactory unitFactory;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (unitType != UnitType.NONE) 
		{
			unitFactory.spawnUnit (unitType);
			unitType = UnitType.NONE;
		}
		
	}
}

